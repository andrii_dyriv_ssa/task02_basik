package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Andrii Dyriv
 * @version 1.0
 * This is the class where began program
 */
public class Application {
    public static void main(String[] args) {
        int beginOfInterval = inputBegginOfInterval();
        int endOfInterval = inputEndOfInterval();
        printOddNumber(beginOfInterval, endOfInterval);
        printEvenNumber(beginOfInterval, endOfInterval);
        printSumEvenNumbers(beginOfInterval, endOfInterval);
        printSumOddNumbers(beginOfInterval, endOfInterval);
        fibonachiSet();
    }

    /**
     * @return begin of user range
     */
    public static int inputBegginOfInterval() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        int beginOfInterval = 0;
        try {
            beginOfInterval = br.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return beginOfInterval;
    }

    /**
     * @return end of user range
     */
    public static int inputEndOfInterval() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        int endOfInterval = 0;
        try {
            endOfInterval = br.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return endOfInterval;
    }

    /**
     * This method output all odd numbers from user range one by one in column from smallest to the biggest
     * @param beginOfInterval
     * @param endOfInterval
     */
    public static void printOddNumber(int beginOfInterval, int endOfInterval) {
        for (int i = beginOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }

    /**
     * This method output all even numbers from user range one by one in column from biggest to the smallest
     * @param beginOfInterval
     * @param endOfInterval
     */
    public static void printEvenNumber(int beginOfInterval, int endOfInterval) {
        for (int i = endOfInterval; i >= beginOfInterval; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void printSumOddNumbers(int beginOfInterval, int endOfInterval) {
        int sumOdd = 0;
        for (int i = beginOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 1) {
                sumOdd += i;
            }
        }
    }

    public static void printSumEvenNumbers(int beginOfInterval, int endOfInterval) {
        int sumEven = 0;
        for (int i = beginOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            }
        }
    }

    /**
     * This method ouput in console the biggest odd number, the biggest even number and persent of even adn odd number
     */
    public static void fibonachiSet() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        int n = 0;
        try {
            n = br.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int countOdd = 0;
        int countEven = 0;
        if (n <= 0) {
            System.out.println("We don’t have any set");
        } else if (n == 1) {
            int F1 = 1;
            countOdd = 1;
            System.out.println(F1 + "- is the biggest odd number");
        } else if (n == 2) {
            int F1 = 1;
            countOdd = 2;
            System.out.println(F1 + "- is the biggest odd number");
        } else {
            int F1 = 1;
            int F2 = 2;
            int a = F1;
            int b = F2;
            countOdd = 2;
            countEven = 1;
            for (int i = 4; i < n; i++) {
                if (i % 2 == 0) {
                    a += b;
                    if (a % 2 == 0) {
                        F2 = a;
                        countEven++;
                    } else {
                        F1 = a;
                        countOdd++;
                    }
                } else {
                    b += a;
                    if (a % 2 == 0) {
                        F2 = b;
                        countEven++;
                    } else {
                        F1 = b;
                        countOdd++;
                    }
                }
                System.out.println(F1 + "- is the biggest odd number");
                System.out.println(F2 + "- is the biggest even number");
            }
        }
        System.out.println("Persantage of odd numbers : " + ((double)countOdd/(countEven+countOdd)));
        System.out.println("Persantage of even numbers : " + ((double)countEven/(countEven+countOdd)));
    }
}
